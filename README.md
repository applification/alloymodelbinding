### Alloy Model Databinding Sample App

This sample app uses the [Random User API](https://randomuser.me) to create a sample app which demonstrates Titanium Alloy Models & Databinding. 
 
[View PDF of Presentation that goes with this Sample App](https://bytebucket.org/applification/alloymodelbinding/raw/ca5a38ecafe5f62413706b5c150efd51be0c0890/DogfishAlloyModelBinding.pdf?token=fafc9c9c362b3983c81b85b9195c63c5373e2095)
 
